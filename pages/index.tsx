import { gql } from '@apollo/client';
import type { NextPage } from 'next';
import Head from 'next/head';
import styled from 'styled-components';

import { Slider } from '../src/components/slider';
import { Program } from '../src/interfaces/program.interface';
import { ApolloProvider } from '../src/utils/apollo-client';

type Props = {
  programs: Program[];
};

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
  background-color: black;
`;

const Home: NextPage<Props> = (props) => {
  return (
    <div>
      <Head>
        <title>TF1 Web front</title>
        <meta
          name="description"
          content="TF1 Web front - Exercice | Interview"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Container>
        <Slider programs={props.programs}></Slider>
      </Container>
    </div>
  );
};

export default Home;

export async function getServerSideProps() {
  const {
    data: { programs },
  } = await ApolloProvider.getClient().query<{
    programs: Program[];
  }>({
    query: gql`
      query Programs {
        programs: program {
          id
          name
          thumbnail: thumnail {
            id
            url
          }
        }
      }
    `,
  });

  return {
    props: {
      programs,
    },
  };
}
