export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  backgrounds: {
    default: 'black',
    values: [
      { name: 'black', value: '#000' },
      { name: 'white', value: '#FFF' },
    ],
  },
  layout: 'centered',
};

//.storybook/preview.js
import { addDecorator } from '@storybook/react';
import GlobalStyle from '../src/styles/global-styles';

addDecorator((story) => (
  <>
    <GlobalStyle />
    {story()}
  </>
));
