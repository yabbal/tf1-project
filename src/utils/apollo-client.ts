import {
  ApolloClient,
  InMemoryCache,
  NormalizedCacheObject,
} from '@apollo/client';

export class ApolloProvider {
  static client: ApolloClient<NormalizedCacheObject>;

  static getClient() {
    return this.client
      ? this.client
      : new ApolloClient({
          uri:
            process.env.API_URL ||
            'https://tf1-interview.hasura.app/v1/graphql',
          cache: new InMemoryCache(),
        });
  }
}
