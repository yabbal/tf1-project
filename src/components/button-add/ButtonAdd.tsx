import { debounce } from 'debounce';
import { FC, useState } from 'react';
import styled from 'styled-components';

type Props = {
  isHover: boolean;
};

const AddButton = styled.div<Props>`
  background: rgba(0, 0, 0, 0.7) url('/assets/add.svg') no-repeat center center;
  background-size: 16px;
  border-radius: 8px;
  width: 32px;
  height: 32px;
  cursor: pointer;
  transition: all 0.2s ease-out;
  &:hover {
    width: 160px;
    background-position: left 8px center;
  }

  display: flex;
  justify-content: flex-end;
  align-items: center;
  align-content: center;
  color: #fff;
  font-size: 14px;
  line-height: 17px;
  text-align: center;

  ${({ isHover }) => isHover && `padding: 0 10px;`}
`;

export const ButtonAdd: FC<{ className?: string }> = (props) => {
  const [isHover, setIsHover] = useState(false);

  const debouncedHandleMouseEnter = debounce(() => setIsHover(true), 200);

  const handleOnMouseLeave = () => {
    setIsHover(false);
    debouncedHandleMouseEnter.clear();
  };

  return (
    <AddButton
      className={props.className}
      isHover={isHover}
      onMouseOver={debouncedHandleMouseEnter}
      onMouseOut={handleOnMouseLeave}
    >
      {isHover ? 'Ajouter à ma liste' : ''}
    </AddButton>
  );
};

export default ButtonAdd;
