import { ComponentMeta, ComponentStory } from '@storybook/react';

import { ButtonAdd } from './ButtonAdd';

export default {
  component: ButtonAdd,
  title: 'Button',
} as ComponentMeta<typeof ButtonAdd>;

const Template: ComponentStory<typeof ButtonAdd> = (args) => (
  <ButtonAdd {...args}></ButtonAdd>
);

export const Add = Template.bind({});
Add.args = {};
