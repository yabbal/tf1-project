import { ComponentMeta, ComponentStory } from '@storybook/react';

import { ButtonSlider } from './ButtonSlider';

export default {
  component: ButtonSlider,
  title: 'Button',
} as ComponentMeta<typeof ButtonSlider>;

const Template: ComponentStory<typeof ButtonSlider> = (args) => (
  <ButtonSlider {...args}></ButtonSlider>
);

export const Chevron = Template.bind({});
Chevron.args = {};
