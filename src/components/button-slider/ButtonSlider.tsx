import { FC } from 'react';
import styled from 'styled-components';

type Props = {
  variant: 'left' | 'right';
  handleClick?: any;
};

const left = '/assets/left-chevron.svg';
const right = '/assets/right-chevron.svg';

const ChevronButton = styled.div<Props>`
  background: #313132
    url(${({ variant }) => (variant === 'left' ? left : right)}) no-repeat
    center center;
  background-size: 17px;
  box-shadow: 0px 2px 4px rgba(38, 40, 47, 0.4);
  width: 48px;
  height: 48px;
  cursor: pointer;
`;

export const ButtonSlider: FC<Props> = ({ variant, handleClick }) => {
  return <ChevronButton variant={variant} onClick={handleClick} />;
};

export default ButtonSlider;
