import { ComponentMeta, ComponentStory } from '@storybook/react';

import { Card } from './Card';

export default {
  component: Card,
  title: 'Card',
} as ComponentMeta<typeof Card>;

const Template: ComponentStory<typeof Card> = (args) => <Card {...args}></Card>;

export const FinalCard = Template.bind({});
FinalCard.args = {
  src: 'https://photos.tf1.fr/700/933/vignette-portrait-fc97cc-43d7cc-0@1x.jpg?overlay=overlay_portrait_nouveau-2a9bc1',
  title: 'Vise le coeur',
};
