import { FC, useState } from 'react';
import styled from 'styled-components';

import { ButtonAdd } from '../button-add';
import { ImageCard } from '../image-card';

type Props = {
  title: string;
  src: string;
};

const SectionCard = styled.div`
  width: 200px;
  margin: 0 12px;
  cursor: pointer;
`;

const HeaderCard = styled.div`
  position: relative;
  height: 266px;
  margin-bottom: 13px;
`;

const ImgCard = styled(ImageCard)`
  transition: all 0.2s ease-out;
  ${SectionCard}:hover & {
    transform: scale(1.03);
  }
`;

const ButtonAddCard = styled(ButtonAdd)`
  position: absolute;
  bottom: 6px;
  right: 6px;
`;

const TitleCard = styled.h2`
  text-overflow: ellipsis;
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  color: #fff;
  font-size: 14px;
  line-height: 17px;
  text-align: center;

  ${SectionCard}:hover & {
    color: #0a78e3;
  }
`;

export const Card: FC<Props> = ({ title, src }) => {
  const [isHover, setIsHover] = useState(false);

  return (
    <SectionCard
      onMouseOver={() => setIsHover(true)}
      onMouseOut={() => setIsHover(false)}
    >
      <HeaderCard>
        <ImgCard src={src} />
        {isHover && <ButtonAddCard />}
      </HeaderCard>
      <TitleCard>{title}</TitleCard>
    </SectionCard>
  );
};

export default Card;
