import { FC, useEffect, useRef, useState } from 'react';
import styled from 'styled-components';

import { Program } from '../../interfaces/program.interface';
import { ButtonSlider } from '../button-slider';
import { Card } from '../card';

const NUMBER_ITEMS = 6;

type Props = {
  programs: Program[];
};

const SliderContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const ButtonSliderContainer = styled.div`
  width: 48px;
`;

const SliderOverflow = styled.div`
  display: flex;
  align-items: center;
  max-width: calc(200px * 6 + 24px * 6);
  min-height: 400px;
  overflow: hidden;
  scroll-behavior: smooth;
`;

const SliderCardsContainer = styled.div`
  display: flex;
`;

export const Slider: FC<Props> = ({ programs }) => {
  const programsRef = useRef<HTMLDivElement[]>([]);

  const [index, setIndex] = useState(0);

  const handleClickPrevious = () => {
    setIndex(index - 1);
    programsRef.current[index - 1].scrollIntoView();
  };

  const handleClickNext = () => {
    setIndex(index + 1);
    programsRef.current[NUMBER_ITEMS + index].scrollIntoView();
  };

  return (
    <SliderContainer>
      <ButtonSliderContainer>
        {index !== 0 && (
          <ButtonSlider variant="left" handleClick={handleClickPrevious} />
        )}
      </ButtonSliderContainer>
      <SliderOverflow>
        <SliderCardsContainer>
          {programs.map((programs, i) => (
            <div key={programs.id} ref={(el) => (programsRef.current[i] = el!)}>
              <Card
                key={programs.id}
                title={programs.name}
                src={programs.thumbnail.url}
              />
            </div>
          ))}
        </SliderCardsContainer>
      </SliderOverflow>
      <ButtonSliderContainer>
        {index + NUMBER_ITEMS < programs.length && (
          <ButtonSlider variant="right" handleClick={handleClickNext} />
        )}
      </ButtonSliderContainer>
    </SliderContainer>
  );
};

export default Slider;
