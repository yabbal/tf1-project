import { ComponentMeta, ComponentStory } from '@storybook/react';

import { Program } from '../../interfaces/program.interface';
import { Slider } from './Slider';

const programs: Program[] = [
  {
    id: 'fdaa50b3-1e68-403d-a595-36d5191f42d2',
    name: 'JLC Family',
    thumbnail: {
      id: 'ce3dbe9d-4916-476a-9dc7-1f370794f51e',
      url: 'https://photos.tf1.fr/700/933/vignette-portrait-7b1f29-6ca786-0@1x.jpg?overlay=overlay_portrait_nouveau-2a9bc1',
    },
  },
  {
    id: '8a6b122f-f6c7-440d-95e9-55de00573425',
    name: 'Ici tout commence',
    thumbnail: {
      id: '8213ff00-c6c3-40ec-aa6a-4f9498f311a4',
      url: 'https://photos.tf1.fr/700/933/vignette-portrait-2-itc-1e0d93-65631e-0@1x.jpg',
    },
  },
  {
    id: '7e4e989d-8a52-438f-8d5d-88f2f23ff51b',
    name: 'Demain nous appartient',
    thumbnail: {
      id: '6e69d04b-8cc5-4a6e-902e-8175459dfb31',
      url: 'https://photos.tf1.fr/700/933/vignette-portrait-dna-rouge-8b3f2c-24782f-0@1x.jpg',
    },
  },
  {
    id: 'a95e1972-6c47-48b4-beb4-d80a568efcad',
    name: 'James bond',
    thumbnail: {
      id: '8e582881-ae1a-4b75-a19c-6510cdd28aae',
      url: 'https://photos.tf1.fr/700/933/vvvignette-portrait-d0af9e-6541ec-0@1x.jpg',
    },
  },
  {
    id: '1095f52d-a6ad-4ccb-9aae-651582b82fcd',
    name: 'Camping Paradis',
    thumbnail: {
      id: 'dd56d650-a9b8-45b2-9a5c-84a91a00398b',
      url: 'https://photos.tf1.fr/700/933/vignette-portrait-camping-paradis-2022-cd4249-12a1a7-0@1x.jpg',
    },
  },
  {
    id: 'f12d0817-5c48-4725-a37e-655834290c2f',
    name: 'La villa des coeurs brisés',
    thumbnail: {
      id: 'c395d991-d3fb-4e6a-9a24-a7129876d6d8',
      url: 'https://photos.tf1.fr/700/933/vignette-portrait-6ce4dc-f39596-0@1x.jpg?overlay=overlay_integrale-ac81b8',
    },
  },
  {
    id: 'a346cad3-266a-4d88-b98b-39fd32818281',
    name: 'Accusé',
    thumbnail: {
      id: 'f6442c28-46bd-4b27-b3e2-a17c84838213',
      url: 'https://photos.tf1.fr/700/933/vignette-portrait-2-accuse-7e2c4a-38cfea-0@1x.jpg?overlay=overlay_portrait_nouveau-2a9bc1',
    },
  },
  {
    id: '59cd3e7c-4c56-4973-a8ff-a4aa8d70edb6',
    name: 'Avatar',
    thumbnail: {
      id: '3e3774ef-2479-437d-bb42-39770bd63fc5',
      url: 'https://photos.tf1.fr/700/933/vvignette-portrait-4cee58-ef7013-0@1x.jpg',
    },
  },
  {
    id: '493eac6e-7303-42e4-8ddb-7647e8b69413',
    name: 'Films TV',
    thumbnail: {
      id: 'b1195bb3-e578-4762-9e1e-96f6e3d647f3',
      url: 'https://photos.tf1.fr/700/933/vignette-portrait-c82a5e-a2eae0-0@1x.jpg',
    },
  },
];

export default {
  component: Slider,
  title: 'Slider',
} as ComponentMeta<typeof Slider>;

const Template: ComponentStory<typeof Slider> = (args) => (
  <Slider {...args}></Slider>
);

export const SliderCards = Template.bind({});
SliderCards.args = {
  programs,
};
