import Image, { ImageLoaderProps } from 'next/image';
import { FC, useState } from 'react';
import styled from 'styled-components';

type Props = {
  src: string;
  className?: string;
};

const loader = ({ src }: ImageLoaderProps) => src;

const CardImage = styled.div`
  & img {
    border-radius: 8px;
  }
`;

export const ImageCard: FC<Props> = (props) => {
  const [src, setSrc] = useState(props.src);

  return (
    <CardImage className={props.className}>
      <Image
        loader={loader}
        src={src}
        width={200}
        height={266}
        alt="Picture of the image card"
        onError={() => setSrc('assets/notFound.jpg')}
      />
    </CardImage>
  );
};

export default ImageCard;
