import { ComponentMeta, ComponentStory } from '@storybook/react';

import { ImageCard } from './ImageCard';

export default {
  component: ImageCard,
  title: 'Card',
} as ComponentMeta<typeof ImageCard>;

const Template: ComponentStory<typeof ImageCard> = (args) => (
  <ImageCard {...args}></ImageCard>
);

export const Image = Template.bind({});
Image.args = {
  src: 'https://photos.tf1.fr/700/933/vignette-portrait-fc97cc-43d7cc-0@1x.jpg?overlay=overlay_portrait_nouveau-2a9bc1',
};
