import { Thumbnail } from './thumbnail.interface';

export interface Program {
  id: string;
  name: string;
  thumbnail: Thumbnail;
}
