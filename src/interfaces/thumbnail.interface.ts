export interface Thumbnail {
  id: string;
  url: string;
}
