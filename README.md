This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app) with Typescript.

Also I use [pnpm](https://pnpm.io/fr/) as package manager.

## Getting Started

First, run the development server:

```bash
pnpm i
pnpm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

To play with the storybook

```bash
pnpm run storybook
```

an new page will be opened or you can visit [http://localhost:6006](http://localhost:6006) to see the result.

## Deployment on Vercel

You can check out the project [here](https://tf1-project.vercel.app/).

### Questions

- Pourquoi il y a un décalage sur le bouton du slider dans la maquette figma ?
- De combien est le pas d’avancement au moment du clic sur le bouton du slider ?
- La pagination doit-elle se faire côté back (en utilisant la pagination de l’api) ou uniquement côté Front en (chargeant toutes les données) ?
- Quelle est la nécessité de certains objets dans le schéma de l’api (exemple: ´image´) ?
- Pourquoi il y a des références circulaires au niveau du schéma ?

### Amélioration possible

- Restructuration des objets dans la maquette sur figma afin de s’y retrouver plus facilement
- Ajout des micros interaction dans les maquettes Figma
- Suppression et renommage de certains champ au niveau de l’api
- Ajouter d’un indicateur afin de pouvoir se situer au niveau du slider
- Version responsive avec un nombre réduit de carte

### Ajout personnel

- Storybook afin de pouvoir tester plus facilement mes différents composant indépendament
- Animation au moment du click sur le bouton du slider
- Animation au moment du hover sur le bouton **+**
- Quelques éléments repris depuis le site mytf1
- Utilisation de Nextjs afin d'avoir une surcouche framework à React
- Styled component pour faire du css-in-js
